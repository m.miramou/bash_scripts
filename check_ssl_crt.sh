#!/usr/bin/env bash

## author Mohammadreza Miramou

echo "
		#################################################################
		        Check ssl certfiacation reminded per days
		#################################################################
		"

domain=$1
curent_day=`date +%s`
dig $1 @8.8.8.8 +answer +short | while read ip  _ _ _ _ 
do
 	end_ssl_date=`echo | openssl s_client -showcerts -servername "$domain" -connect "$ip":443 2>/dev/null | openssl x509 -inform pem -noout -enddate |awk -F '=' '{print $2}'`
	expiry_day=`date -d "$end_ssl_date" +%s`
	remine_days="$(( ("$expiry_day" - "$curent_day") / (3600 * 24) ))"
	echo "$remine_days"
done
