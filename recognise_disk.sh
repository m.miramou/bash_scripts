#!/usr/bin/env bash

## Recognise new hard disks,resized exist disks in linux os
if [ `whoami` != "root" ]
then
	echo "Error::Please run this script with root privilege...!!!"
	exit 1
else
	read -p "Please enter mode for recognised disk(new/resize):: " mode
fi
case $mode in 
	new)
		for i in `ls /sys/class/scsi_host`
		do
			echo "- - -" > /sys/class/scsi_host/$i/scan
		done
		partprobe
		;;
	resize)
		for i in `ls /sys/class/scsi_disk`
		do
			echo "1" > /sys/class/scsi_disk/$i/device/rescan
		done
		partprobe
		;;
esac
