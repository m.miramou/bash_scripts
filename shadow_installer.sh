#!/usr/bin/env bash
### Install and configuration shadow vpn server on linux distribution

# Variables
OS_DIST=`hostnamectl | grep Operating | awk '{print $3}'`
WORK_DIR="/opt"
IF_NAME=`route | grep default | head -1 | awk '{print $8}'`


# Check root access for run this program
if [ "$UID" != 0 ]
then 
	echo "$USER Not soudoer, Please run this program with root privilege"
	exit 1
fi

ubuntu_setup(){
echo "
	           /$$                             /$$                                           /$$                        
          | $$                            | $$                                          | $$                        
 /$$   /$$| $$$$$$$  /$$   /$$ /$$$$$$$  /$$$$$$   /$$   /$$        /$$$$$$$  /$$$$$$  /$$$$$$   /$$   /$$  /$$$$$$ 
| $$  | $$| $$__  $$| $$  | $$| $$__  $$|_  $$_/  | $$  | $$       /$$_____/ /$$__  $$|_  $$_/  | $$  | $$ /$$__  $$
| $$  | $$| $$  \ $$| $$  | $$| $$  \ $$  | $$    | $$  | $$      |  $$$$$$ | $$$$$$$$  | $$    | $$  | $$| $$  \ $$
| $$  | $$| $$  | $$| $$  | $$| $$  | $$  | $$ /$$| $$  | $$       \____  $$| $$_____/  | $$ /$$| $$  | $$| $$  | $$
|  $$$$$$/| $$$$$$$/|  $$$$$$/| $$  | $$  |  $$$$/|  $$$$$$/       /$$$$$$$/|  $$$$$$$  |  $$$$/|  $$$$$$/| $$$$$$$/
 \______/ |_______/  \______/ |__/  |__/   \___/   \______/       |_______/  \_______/   \___/   \______/ | $$____/ 
                                                                                                          | $$      
                                                                                                          | $$      
                                                                                                          |__/
													  "

# Update and upgrade packages
apt update -y && apt upgrade-yuf
# Installing prerequesties package
apt install -y --no-install-recommends gettext build-essential autoconf libtool libpcre3-dev \
                                       asciidoc xmlto libev-dev libudns-dev automake libmbedtls-dev \
				       libsodium-dev git python-m2crypto libc-ares-dev

# Cloning progect from github repository
cd "$WORK_DIR" && git clone https://github.com/shadowsocks/shadowsocks-libev.git && cd shadowsocks-libev
git submodule update --init --recursive

# Install shadowsocks-libev
./autogen.sh
./configure
make && make install
# Configuration shadowsocks

adduser --system --no-create-home --group shadowsocks
mkdir -m 775 /etc/shadowsocks
# Get some information from user
read -p "Please Enter your IP address :: " ip
read -s -p "Pleas enter your costum password :: " pass
cat > /etc/shadowsocks/shadowsocks.json << EOL
{
    "server":"$ip",
    "server_port":8378,
    "password":"$pass",
    "timeout":300,
    "method":"aes-256-gcm",
    "fast_open": true
}
EOL
# Uptimaizing shadowsocks
cat > /etc/sysctl.d/shadow.conf << EOL
# max open files
fs.file-max = 51200
# max read buffer
net.core.rmem_max = 67108864
# max write buffer
net.core.wmem_max = 67108864
# default read buffer
net.core.rmem_default = 65536
# default write buffer
net.core.wmem_default = 65536
# max processor input queue
net.core.netdev_max_backlog = 4096
# max backlog
net.core.somaxconn = 4096
# resist SYN flood attacks
net.ipv4.tcp_syncookies = 1
# reuse timewait sockets when safe
net.ipv4.tcp_tw_reuse = 1
# turn off fast timewait sockets recycling
net.ipv4.tcp_tw_recycle = 0
# short FIN timeout
net.ipv4.tcp_fin_timeout = 30
# short keepalive time
net.ipv4.tcp_keepalive_time = 1200
# outbound port range
net.ipv4.ip_local_port_range = 10000 65000
# max SYN backlog
net.ipv4.tcp_max_syn_backlog = 4096
# max timewait sockets held by system simultaneously
net.ipv4.tcp_max_tw_buckets = 5000
# turn on TCP Fast Open on both client and server side
net.ipv4.tcp_fastopen = 3
# TCP receive buffer
net.ipv4.tcp_rmem = 4096 87380 67108864
# TCP write buffer
net.ipv4.tcp_wmem = 4096 65536 67108864
# turn on path MTU discovery
net.ipv4.tcp_mtu_probing = 1
# for high-latency network
net.ipv4.tcp_congestion_control = hybla
# for low-latency network, use cubic instead
net.ipv4.tcp_congestion_control = cubic
EOL
sysctl --system > /dev/null

# create shadowsocks systemd service
cat > /etc/systemd/system/shadowsocks.service << EOL
[Unit]
Description=Shadowsocks proxy server

[Service]
User=root
Group=root
Type=simple
ExecStart=/usr/local/bin/ss-server -c /etc/shadowsocks/shadowsocks.json -a shadowsocks -v start
ExecStop=/usr/local/bin/ss-server -c /etc/shadowsocks/shadowsocks.json -a shadowsocks -v stop

[Install]
WantedBy=multi-user.target
EOL
# Reload systemd daemon 
systemctl daemon-reload
# enable and start shadowsocks service
systemctl enable shadowsocks >/dev/null && systemctl start shadowsocks
# Allow shadowsocks port in firewall
ufw allow proto tcp to 0.0.0.0/0 port 8378 comment "Shadowsocks server listen port"
ufw enable

echo -e "\tinstallation is complated"

}

centos_setup() {
echo "
	  /$$$$$$                        /$$                                                     /$$                        
 /$$__  $$                      | $$                                                    | $$                        
| $$  \__/  /$$$$$$  /$$$$$$$  /$$$$$$    /$$$$$$   /$$$$$$$        /$$$$$$$  /$$$$$$  /$$$$$$   /$$   /$$  /$$$$$$ 
| $$       /$$__  $$| $$__  $$|_  $$_/   /$$__  $$ /$$_____/       /$$_____/ /$$__  $$|_  $$_/  | $$  | $$ /$$__  $$
| $$      | $$$$$$$$| $$  \ $$  | $$    | $$  \ $$|  $$$$$$       |  $$$$$$ | $$$$$$$$  | $$    | $$  | $$| $$  \ $$
| $$    $$| $$_____/| $$  | $$  | $$ /$$| $$  | $$ \____  $$       \____  $$| $$_____/  | $$ /$$| $$  | $$| $$  | $$
|  $$$$$$/|  $$$$$$$| $$  | $$  |  $$$$/|  $$$$$$/ /$$$$$$$/       /$$$$$$$/|  $$$$$$$  |  $$$$/|  $$$$$$/| $$$$$$$/
 \______/  \_______/|__/  |__/   \___/   \______/ |_______/       |_______/  \_______/   \___/   \______/ | $$____/ 
                                                                                                          | $$      
                                                                                                          | $$      
                                                                                                          |__/
													  "

# Update os package
yum update -y && yum upgrade -y
# Install prerequestiest package
yum install epel-release -y
yum install -y gcc gettext autoconf libtool automake make pcre-devel asciidoc xmlto udns-devel \
               libev-devel libsodium-devel mbedtls-devel git m2crypto c-ares-devel
# Clone shadowsocks repository from github
cd "$WORKDIR" && git clone https://github.com/shadowsocks/shadowsocks-libev.git && cd shadowsocks-libev
git submodule update --init --recursive

# installing 
./autogen.sh
./configure
make && make install

# Configuration shadowsocks steps
adduser --system --no-create-home --shell /bin/false shadowsocks

# Create shadow config directory 
mkdir -m 775 /etc/shadowsocks
# Get some information from user
read -p "Please enter your IP address:: " ip 
read -s -p "Please enter your custom password:: " pass
cat > /etc/shadowsocks/shadowsocks.json << EOL
{
    "server":"$ip",
    "server_port":8378,
    "password":"$pass",
    "timeout":300,
    "method":"aes-256-gcm",
    "fast_open": true
}

EOL
# Optimize shadow server
cat > /etc/sysctl.d/shadow.conf << EOL
# max open files
fs.file-max = 51200
# max read buffer
net.core.rmem_max = 67108864
# max write buffer
net.core.wmem_max = 67108864
# default read buffer
net.core.rmem_default = 65536
# default write buffer
net.core.wmem_default = 65536
# max processor input queue
net.core.netdev_max_backlog = 4096
# max backlog
net.core.somaxconn = 4096
# resist SYN flood attacks
net.ipv4.tcp_syncookies = 1
# reuse timewait sockets when safe
net.ipv4.tcp_tw_reuse = 1
# turn off fast timewait sockets recycling
net.ipv4.tcp_tw_recycle = 0
# short FIN timeout
net.ipv4.tcp_fin_timeout = 30
# short keepalive time
net.ipv4.tcp_keepalive_time = 1200
# outbound port range
net.ipv4.ip_local_port_range = 10000 65000
# max SYN backlog
net.ipv4.tcp_max_syn_backlog = 4096
# max timewait sockets held by system simultaneously
net.ipv4.tcp_max_tw_buckets = 5000
# turn on TCP Fast Open on both client and server side
net.ipv4.tcp_fastopen = 3
# TCP receive buffer
net.ipv4.tcp_rmem = 4096 87380 67108864
# TCP write buffer
net.ipv4.tcp_wmem = 4096 65536 67108864
# turn on path MTU discovery
net.ipv4.tcp_mtu_probing = 1
# for high-latency network
net.ipv4.tcp_congestion_control = hybla
# for low-latency network, use cubic instead
net.ipv4.tcp_congestion_control = cubic
EOL
sysctl --system > /dev/null
# Create shadowsocks service
cat > /etc/systemd/system/shadowsocks.service << EOL
[Unit]
Description=Shadowsocks proxy server

[Service]
User=root
Group=root
Type=simple
ExecStart=/usr/local/bin/ss-server -c /etc/shadowsocks/shadowsocks.json -a shadowsocks -v start
ExecStop=/usr/local/bin/ss-server -c /etc/shadowsocks/shadowsocks.json -a shadowsocks -v stop

[Install]
WantedBy=multi-user.target
EOL
# reload systemd daemon
systemctl daemon-reload
# Enable and start shadowsocks service
systemctl enable shadowsocks >/dev/null && systemctl start shadowsocks
# Firewall config
systemctl start firewalld
firewall-cmd --permanent --zone=public --add-rich-rule='rule family="ipv4" port protocol="tcp" port="8378" accept'
firewall-cmd --reload

}
case $OS_DIST in
	Ubuntu)
		ubuntu_setup;;
	CentOS)
		centos_setup;;
	*)
		echo -e "\tSory this installation is not compatible with your OS\nThis program for just ubuntu and centos operation systems"
		exit 1;;

	esac
